package com.ss.friends.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.ss.friends.R
import com.ss.friends.data.AppSessions


class UserFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // layout variables

        var full_name = view!!.findViewById<TextView>(R.id.full_names)
        var address = view!!.findViewById<TextView>(R.id.address)
        var city = view!!.findViewById<TextView>(R.id.city)
        var state = view!!.findViewById<TextView>(R.id.state)
        var country = view!!.findViewById<TextView>(R.id.country)
        var email = view!!.findViewById<TextView>(R.id.email)
        var phone = view!!.findViewById<TextView>(R.id.phone)

        // function call for updating ui data
        init()
    }

    fun init()
    {
        var full_name = view!!.findViewById<TextView>(R.id.full_names)
        var address = view!!.findViewById<TextView>(R.id.address)
        var city = view!!.findViewById<TextView>(R.id.city)
        var state = view!!.findViewById<TextView>(R.id.state)
        var country = view!!.findViewById<TextView>(R.id.country)
        var email = view!!.findViewById<TextView>(R.id.email)
        var phone = view!!.findViewById<TextView>(R.id.phone)
        try {

            full_name.text = AppSessions.users.results[AppSessions.position].name.first+" "+AppSessions.users.results[AppSessions.position].name.last
            address.text = AppSessions.users.results[AppSessions.position].location.street.name+","+AppSessions.users.results[AppSessions.position].location.street.number.toString()
            state.text = AppSessions.users.results[AppSessions.position].location.state
            city.text = AppSessions.users.results[AppSessions.position].location.city
            country.text = AppSessions.users.results[AppSessions.position].location.country
            email.text = AppSessions.users.results[AppSessions.position].email
            phone.text = AppSessions.users.results[AppSessions.position].phone

            // email onclick

            email.setOnClickListener()
            {
                val intent = Intent(Intent.ACTION_VIEW)
                val data: Uri = Uri.parse(
                    "mailto:${AppSessions.users.results[AppSessions.position].email}?subject=" + Uri.encode("subject")
                        .toString() + "&body=" + Uri.encode("body")
                )
                intent.data = data
                startActivity(intent)
            }

        }catch (e:Exception)
        {
            ;
        }
    }
}