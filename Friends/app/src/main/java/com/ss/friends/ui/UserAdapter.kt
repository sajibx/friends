package com.ss.friends.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.ss.friends.R
import com.ss.friends.data.AppSessions
import com.ss.friends.data.Result
import com.ss.friends.fragments.UserFragment
import com.ss.friends.fragments.UserListFragment

class UserAdapter(var results: List<Result>, var fragmentManager: FragmentManager) :RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return UserAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        var data = results[position]
        holder.full_name.text = data.name.first+" "+data.name.last
        holder.country.text = data.location.country
        holder.layout.setOnClickListener()
        {
            AppSessions.position = position
            val transaction = fragmentManager!!.beginTransaction()
            var fragment = UserFragment()
            transaction.replace(R.id.univarsal, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    override fun getItemCount(): Int {
        return results.size
    }

    class ViewHolder(item:View):RecyclerView.ViewHolder(item)
    {
        var full_name = item.findViewById(R.id.full_name) as TextView
        var country = item.findViewById(R.id.country) as TextView
        var layout = item.findViewById(R.id.layouts) as ConstraintLayout
    }
}