package com.ss.friends

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ss.friends.fragments.UserListFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val transaction = this.supportFragmentManager!!.beginTransaction()
        var fragment = UserListFragment()
        transaction.replace(R.id.univarsal, fragment)
        //transaction.addToBackStack(null)
        transaction.commit()
    }
}