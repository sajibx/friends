package com.ss.friends.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.httpGet
import com.ss.friends.R
import com.ss.friends.data.AppSessions
import com.ss.friends.data.Users
import com.ss.friends.ui.UserAdapter
import kotlinx.coroutines.launch
import java.lang.Exception

class UserListFragment : BaseFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }


    fun init()
    {
        // thread call

        launch {

            // api call

            "https://randomuser.me/api/?results=10"
                .httpGet()
                .header("Content-Type" to "application/json")
                .responseObject(Users.Deserializer())
                { request, response, result ->

                    when (result) {
                        is com.github.kittinunf.result.Result.Failure -> {

                            val ex = result.getException()

                        }
                        is com.github.kittinunf.result.Result.Success -> {

                            // caching user list data
                            AppSessions.users = result.value

                            // updating ui with api data

                            updateUI {

                                try{

                                    // list genarating

                                    val recycler = view!!.findViewById<RecyclerView>(R.id.recycler)
                                    recycler.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?

                                    val adapter = UserAdapter(AppSessions.users.results, fragmentManager!!)

                                    recycler.setHasFixedSize(true);
                                    recycler.adapter = adapter

                                }catch (e:Exception)
                                {
                                    ;
                                }

                            }


                        }


                    }
                }

        }
    }

}